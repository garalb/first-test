**Code version:**

(The version in which the problem was found. We can't assume
that you have the latest version)

**Summary:**

(Summarize the bug encountered concisely. Try to use also a descriptive title for the issue)

**Steps to reproduce:**

(How one can reproduce the issue - this is very important)

**Example Project:**

(If possible, please create an example project here on GitLab.com that
exhibits the problematic behaviour, and link to it here in the bug
report. For example, you could put in the project input and output files)

**What is the current bug behavior?**

(What actually happens)

**What is the expected correct behavior?**

(What you should see instead)

**Relevant logs and/or screenshots**

(Paste any relevant logs - please use code blocks (```) to format console output,
logs, and code as it's very hard to read otherwise.)

**Possible fixes**

(If you can, link to the line of code that might be responsible for the problem)

/label ~bug_report
/assign @garalb
