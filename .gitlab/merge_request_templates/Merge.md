**Describe what this merge does**

- [ ] Add a single line to Docs/ChangeLog.md describing the change
- [ ] Make a thorough description of the changes here
- [ ] If the merge request affects options, update `Docs/siesta.tex`
- [ ] ... OTHERS?
